﻿using FoodJewel.Models.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Tests.Tests
{
    [TestClass]
    public class JournalUser_ODataTests : BaseTestServer
    {
        [ClassInitialize]
        public new static void Initialize(TestContext context)
        {
            BaseTestServer.Initialize(context);
        }

        [ClassCleanup]
        public new static void Cleanup()
        {
            BaseTestServer.Cleanup();
        }

        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CanOnlySeeSelf()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Call should complete succesfully.");
            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.IsNotNull(result.value, "Should have a result.");
                Assert.IsTrue(result.value.Count == 1, "There can be only one.");
                Assert.AreEqual((string)result.value[0].UserName, userName, "JournalUser should have the current users name.");
            }

            DeleteUser(userName);
        }

        /// <summary>
        /// When an administrator queries the JournalUser OData endpoint without parameters, 
        /// they should receive a list of all JournalUsers (in addition to the currently logged in 'admin')
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_AdminCanSeeMultipleUsers()
        {
            //Call get JournalUsers
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Call should complete succesfully.");
            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.IsNotNull(result.value, "Should have a result.");
                Assert.IsTrue(result.value.Count > 1, "There should be more than 1 result.");

                //admin is an Application user, and doesn't have a matching JournalUser record, so we shouldn't see him here
                Assert.IsTrue(((JArray)result.value)
                    .Select(r => (string)r["UserName"])
                    .Any(r => !string.Equals(r, "admin", StringComparison.CurrentCultureIgnoreCase)),
                        "JournalUser list should have other users in addition to the currently logged in 'admin'.");
            }

        }



        /// <summary>
        /// When an administrator queries the JournalUser OData endpoint default server driven paging is in effect
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_GetDefaultsServerDrivenODataPaging()
        {
            //Call get JournalUsers
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var users = from idx in Enumerable.Range(0, 120)
                        select new
                            {
                                UserName = string.Format("serverdrivenpaging{0}", idx),
                                FullName = "Post Full Name Test",
                                Birthday = DateTime.Now,
                                CalorieGoal = 0,
                                Height = new { Value = 0M, Format = "cm" },
                                LastWeight = new { Value = 0M, Format = "kg" }
                            };

            HttpResponseMessage response = null;
            List<string> userLocs = new List<string>();
            foreach (var usr in users)
            {
                response = client.PostAsJsonAsync("/odata/JournalUsers", usr).Result;
                Assert.IsTrue(response.IsSuccessStatusCode, "Call for user {0} should complete succesfully.", usr.UserName);
                if (response.IsSuccessStatusCode)
                    userLocs.Add(response.Headers.Location.AbsolutePath);   //Get the newly inserted objects location, so we can delete them later
            }

            response = client.GetAsync("/odata/JournalUsers?$count=true").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Call should complete succesfully.");

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.IsTrue(result["@odata.count"].Value >=120, "Should retrieve full count of results");
                Assert.IsTrue((result.value as JArray).Count == 100, "Should only retrieve the first 100 entries.");
                Assert.IsNotNull(result["@odata.nextLink"], "Query should provide a link to the next page.");
            }

            foreach (var usr in userLocs)
            {
                response = client.DeleteAsync(usr).Result;
                Assert.IsTrue(response.IsSuccessStatusCode, "Delete of {0} should succeed.", usr);
            }

        }

        /// <summary>
        /// When an administrator queries the JournalUser OData endpoint with Client-Driven paging, 
        /// next link and count should be returned.  (NOTE:  Server limit of 100 is also in effect)
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_GetSupportsClientDrivenODataPaging()
        {
            //Call get JournalUsers
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var users = from idx in Enumerable.Range(0, 20)
                        select new
                        {
                            UserName = string.Format("clientdrivenpaging{0}", idx),
                            FullName = "Post Full Name Test",
                            Birthday = DateTime.Now,
                            CalorieGoal = 0,
                            Height = new { Value = 0M, Format = "cm" },
                            LastWeight = new { Value = 0M, Format = "kg" },
                        };

            HttpResponseMessage response = null;
            List<string> userLocs = new List<string>();
            foreach (var usr in users)
            {
                response = client.PostAsJsonAsync("/odata/JournalUsers", usr).Result;
                Assert.IsTrue(response.IsSuccessStatusCode, "Call for user {0} should complete succesfully.", usr.UserName);
                if (response.IsSuccessStatusCode)
                    userLocs.Add(response.Headers.Location.AbsolutePath);   //Get the newly inserted objects location, so we can delete them later
            }

            response = client.GetAsync("/odata/JournalUsers?$top=10&$count=true").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Call should complete succesfully.");

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.IsTrue(result["@odata.count"].Value >= 10, "Should retrieve full count of results");
                Assert.IsTrue((result.value as JArray).Count == 10, "Should only retrieve the first 10 entries.");
                Assert.IsNull(result["@odata.nextLink"], "Client driven paging does not provide a link to the next page.");
            }

            foreach (var usr in userLocs)
            {
                response = client.DeleteAsync(usr).Result;
                Assert.IsTrue(response.IsSuccessStatusCode, "Delete of {0} should succeed.", usr);
            }

        }

        /// <summary>
        /// When an administrator filters a JournalUser OData query by username, they should receive exactly one result
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_AdminODataFilterByUserName()
        {
            //Call get JournalUsers
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers?$filter=UserName+eq+'unittest'").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Call should complete succesfully.");
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(result.value, "Should have a result.");
            Assert.IsTrue(result.value.Count == 1, "There should be exactly 1 result.");
            Assert.AreEqual((string)result.value[0].UserName, "unittest", "Must have found 'unittest' user.");
        }

        /// <summary>
        /// A user can update their own date, including username, full name, and birthday (as UTC DateTimeOffset)
        /// When the JournalUser username is changed, 
        /// the user must be able to:
        ///     *   Use the new username for authentication
        ///     *   Recieve the updated data from the JournalUser service.
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CanPutSelf()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Make up new user name
            userName = userName.Replace("unittestregister", "PutTest");
            var userFullName = "Put Test User";
            var userBDay = DateTimeOffset.Parse("12/25/1965").ToUniversalTime();

            //Modify user and put back
            dynamic user = results.value[0];

            //All offsets are ignored in MySql so make sure to push offset into DateTime component
            user.Birthday = userBDay;
            user.FullName = userFullName;
            user.UserName = userName;
            response = client.PutAsJsonAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id), (JObject)user).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully put user.");

            //Must be able to log in with new user name
            token = LoginUser(userName, password);

            //Get user, and make sure other fields are changed
            client = server.HttpClient;
            //We've changed the user name, make sure to use the new login token.
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get new user.");
            results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");
            Assert.AreEqual((string)user.FullName, userFullName, "Full Name must be changed.");
            Assert.AreEqual((DateTimeOffset)user.Birthday, userBDay, "Birthday must be changed.");

            //Clean up, delete thsi user
            DeleteUser(userName);
        }

        /// <summary>
        /// A non-administrative user cannot put updates to a different user
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CantPutOthers()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Make up new user name
            var oldUserName = userName;
            userName = userName.Replace("unittestregister", "PutTest");

            //Modify user and put back
            dynamic user = results.value[0];

            //All offsets are ignored in MySql so make sure to push offset into DateTime component
            user.Birthday = DateTimeOffset.Parse("12/25/1965").ToUniversalTime();
            user.FullName = "Put Test User";
            user.UserName = userName;

            //Try putting as a different user
            client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            response = client.PutAsJsonAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id), (JObject)user).Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "Putting another user should fail.");

            //Clean up, delete thsi user
            DeleteUser(oldUserName);
        }

        /// <summary>
        /// An administrative user can put updates for anyone
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_AdminCanPutOthers()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);

            //Call get JournalUsers
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Find newly registered user
            JArray arry = (JArray)results.value;
            dynamic user = arry.First(r => r["UserName"].Value<string>() == userName);

            //Make up new user name
            userName = userName.Replace("unittestregister", "PutTest");

            //Modify user and put back

            //All offsets are ignored in MySql so make sure to push offset into DateTime component
            user.Birthday = DateTimeOffset.Parse("12/25/1965").ToUniversalTime();
            user.FullName = "Put Test User";
            user.UserName = userName;
            response = client.PutAsJsonAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id), (JObject)user).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully put user.");

            //Must be able to log in with new user name
            var token = LoginUser(userName, password);


            //Clean up, delete this user
            DeleteUser(userName);
        }

        /// <summary>
        /// Anyone can delete themselves from JournalUsers.
        /// NOTE:  This does not remove the login.  That would use the DELETE api/account/userinfo/{username?} endpoint
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CanDeleteSelf()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Send a delete command
            dynamic user = results.value[0];
            response = client.DeleteAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id)).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully put user.");

            //List should now be empty
            response = client.GetAsync("/odata/JournalUsers").Result;
            results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsTrue(results.value.Count == 0, "JournalUsers list should be empty.");

            //Logging in should recreate JournalUser
            token = LoginUser(userName, password);  //Since we haven't changed the user name, we can keep using the old token for now.
            response = client.GetAsync("/odata/JournalUsers").Result;
            results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsTrue(results.value.Count == 1, "JournalUsers list should contain 1 entry.");

            //Clean up, delete this user
            DeleteUser(userName);
        }

        /// <summary>
        /// A non-administrative use cannot delete the JournalUser data of other users
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CantDeleteOthers()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Send a delete command
            dynamic user = results.value[0];

            //Change to other user
            client = UserClient;
            response = client.DeleteAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id)).Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "Delete request for other user should fail.");

            //Clean up, delete this user
            DeleteUser(userName);
        }

        /// <summary>
        /// An administrator can delete the JournalUser data for anyone
        /// NOTE:  This doesn't remove their logins, just wipes out all of their data, through cascading deletes
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_AdminCanDeleteOthers()
        {
            //Create Temp User
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Call get JournalUsers
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("/odata/JournalUsers").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Must succesfully get user.");
            dynamic results = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNotNull(results.value, "Results must have value");

            //Send a delete command
            dynamic user = results.value[0];

            //Change to other user
            client = AdminClient;
            response = client.DeleteAsync(string.Format("/odata/JournalUsers({0})", (string)user.Id)).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Delete request for other user should succeed.");

            //Clean up, delete this user
            DeleteUser(userName);
        }

        /// <summary>
        /// A non-administrative user can't make a POST call for two different reasons.
        /// First, if they try to POST their own data again, referential integrity won't allow it.
        /// If they try to POST a totally new record, not for themselves, 
        /// then they should fail because they aren't administrators.
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_CantPost()
        {

            var client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Try posting yourself
            var user = new
            {
                UserName = "unittest",
                FullName = "Post FUll Name Test",
                Birthday = DateTime.Now,
                CalorieGoal = 0,
                Height = new { Value = 0M, Format = "cm" },
                LastWeight = new { Value = 0M, Format = "kg" }
            };

            var response = client.PostAsJsonAsync("/odata/JournalUsers", user).Result;

            //You already exist, so this should fail for two different reasons (permissions, and db)
            Assert.IsFalse(response.IsSuccessStatusCode, "Post for self should fail.");

            var user2 = Activator.CreateInstance(user.GetType(), 
                string.Format("unittestregister{0}", Guid.NewGuid().ToString("N")), 
                "Full Name", 
                user.Birthday, 
                user.CalorieGoal, 
                user.Height, 
                user.LastWeight);

            //Try posting again, this should fail as well.
            response = client.PostAsJsonAsync("/odata/JournalUsers", user2).Result;

            //You shouldn't have permissions to do this
            Assert.IsFalse(response.IsSuccessStatusCode, "Post for new user should fail.");

        }

        /// <summary>
        /// Administrative users can make POST entries, but cannot violate referential integrity.
        /// If the UserName already exists, the POST should fail.
        /// </summary>
        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_AdminCanPost()
        {

            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Try posting yourself
            var user = new
            {
                UserName = "unittest",
                FullName = "Post FUll Name Test",
                Birthday = DateTime.Now,
                CalorieGoal = 0,
                Height = new { Value = 0M, Format = "cm" },
                LastWeight = new { Value = 0M, Format = "kg" }
            };

            var response = client.PostAsJsonAsync("/odata/JournalUsers", user).Result;

            //User already exists, post should fail because of DB constraints
            Assert.IsFalse(response.IsSuccessStatusCode, "Post for existing user should fail.");

            var user2 = Activator.CreateInstance(user.GetType(), 
                string.Format("unittestregister{0}", 
                Guid.NewGuid().ToString("N")), 
                "Full Name", 
                user.Birthday, 
                user.CalorieGoal, 
                user.Height, 
                user.LastWeight);

            //Try posting again, this should succeed for administrators
            response = client.PostAsJsonAsync("/odata/JournalUsers", user2).Result;
            //You shouldn't have permissions to do this
            Assert.IsTrue(response.IsSuccessStatusCode, "Post for new user should succeed.");

            //Cleanup
            var result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            response = client.DeleteAsync(string.Format("/odata/JournalUsers({0})", result["Id"].Value<int>())).Result;

        }

        [TestMethod]
        [TestCategory("JournalUser")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void JournalUser_PlaceHolder()
        {
        }
    }
}
