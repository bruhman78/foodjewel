﻿using FoodJewel.Models.Domain;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.OData;
using Microsoft.AspNet.Identity;
using FoodJewel.Code;
using System.Web.OData.Routing;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Controllers.OData
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using FoodJewel.Models.Domain;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<WeightEntry>("WeightEntries");
    builder.EntitySet<JournalUser>("JournalUsers"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [Authorize]
    public class WeightEntriesController : ODataController
    {
        private ApplicationUserManager userManager;
        private ApplicationDbContext dbContext;

        public WeightEntriesController(ApplicationUserManager userManager, ApplicationDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }

        // GET: odata/WeightEntries
        [EnableQuery(PageSize = 100)]
        public async Task<IQueryable<WeightEntry>> GetWeightEntries()
        {
            IQueryable<WeightEntry> qry = null;
            if (User.IsInRole("Administrators"))
            {
                qry = dbContext.WeightEntries;
            }
            else
            {
                string userId = User.Identity.GetUserId();
                var journalUser = await dbContext.JournalUsers.SingleAsync(u => u.IdentityUserId == userId);
                qry = dbContext.WeightEntries.Where(e => e.UserId == journalUser.Id);
            }
            return qry;
        }

        // GET: odata/WeightEntries(5)
        [EnableQuery]
        public async Task<IHttpActionResult> GetWeightEntry([FromODataUri] int key)
        {
            var qry = dbContext.WeightEntries.Where(weightEntry => weightEntry.Id == key);

            if (!User.IsInRole("Administrators"))
            {
                string identityId = User.Identity.GetUserId();
                var journalUser = await dbContext.JournalUsers.SingleOrDefaultAsync(u=>u.IdentityUserId == identityId);
                qry = qry.Where(e => e.UserId == journalUser.Id);
            }

            if (qry.Count() == 0)
            {
                return NotFound();
            }

            return Ok(SingleResult.Create(qry));
        }

        // PUT: odata/WeightEntries(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, WeightEntry weightEntry)
        {
            //TODO:  Add Administration filtering
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != weightEntry.Id)
            {
                return BadRequest();
            }

            dbContext.Entry(weightEntry).State = EntityState.Modified;

            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeightEntryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(weightEntry);
        }

        // POST: odata/WeightEntries
        public async Task<IHttpActionResult> Post(WeightEntry weightEntry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string userId = User.Identity.GetUserId();
            var journalUser = await dbContext.JournalUsers.SingleOrDefaultAsync(u=>u.IdentityUserId == userId);

            //If not an administrator, you better be trying to add yourself
            if (!User.IsInRole("Administrators")
                && weightEntry.UserId != 0
                && weightEntry.UserId != journalUser.Id)
            {
                return this.ForbiddenResult("Only administrators may update other JournalUsers.");
            }

            if (weightEntry.UserId == 0)
            {
                if (journalUser != null)
                {
                    weightEntry.UserId = journalUser.Id;
                }
                else
                {
                    var state = new ModelStateDictionary();
                    state.AddModelError("", "Must Provide UserId.");
                    return this.BadRequest(state);
                }
            }

            dbContext.WeightEntries.Add(weightEntry);
            await dbContext.SaveChangesAsync();

            return Created(weightEntry);
        }

        // PATCH: odata/WeightEntries(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<WeightEntry> patch)
        {
            //TODO:  Add Administration filtering
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            WeightEntry weightEntry = await dbContext.WeightEntries.FindAsync(key);
            if (weightEntry == null)
            {
                return NotFound();
            }

            patch.Patch(weightEntry);

            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeightEntryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(weightEntry);
        }

        // DELETE: odata/WeightEntries(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            WeightEntry weightEntry = await dbContext.WeightEntries.FindAsync(key);
            if (weightEntry == null)
            {
                return NotFound();
            }


            string userId = User.Identity.GetUserId();

            //If not an administrator, you better be trying to add yourself
            if (!User.IsInRole("Administrators"))
            {
                var journalUser = await dbContext.JournalUsers.SingleOrDefaultAsync(u => u.IdentityUserId == userId);
                if (weightEntry.UserId != journalUser.Id)
                    return this.ForbiddenResult("Only administrators may delete other JournalUsers.");
            }

            dbContext.WeightEntries.Remove(weightEntry);
            await dbContext.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {

            base.Dispose(disposing);
        }

        private bool WeightEntryExists(int key)
        {
            return dbContext.WeightEntries.Count(e => e.Id == key) > 0;
        }
    }
}
