﻿using FoodJewel.Code;
using FoodJewel.Models.Binding;
using FoodJewel.Models.Domain;
using FoodJewel.Models.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager userManager;
        private ApplicationDbContext dbContext;

        public AccountController(ApplicationUserManager userManager, ApplicationDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(IdentityUserBindingModel model)

        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.UserName, FullName = model.FullName, Email = model.Email };

            IdentityResult result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return this.GetErrorResult(result);
            }
            await user.SynchronizeJournalUser(dbContext);

            return Ok();
        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return this.GetErrorResult(result);
            }
            return Ok();
        }

        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await userManager.FindByNameAsync(model.UserName);

            if (model.UserName == null
                || (!string.Equals(User.Identity.GetUserName(), model.UserName, StringComparison.CurrentCultureIgnoreCase)
                    && !User.IsInRole("Administrators")))
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
            }

            var token = await userManager.GeneratePasswordResetTokenAsync(user.Id);
            var result = await userManager.ResetPasswordAsync(user.Id, token, model.Password);

            if (!result.Succeeded)
            {
                return this.GetErrorResult(result);
            }
            return Ok();
        }

        [Route("UserInfo/{userName?}")]
        public async Task<UserInfoViewModel> GetUserInfo(string userName=null)
        {
            if (userName != null
                && !string.Equals(User.Identity.GetUserName(), userName, StringComparison.CurrentCultureIgnoreCase)
                && !User.IsInRole("Administrators"))
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
            }

            var user = await userManager.FindByNameAsync(userName??User.Identity.GetUserName());

            if (user != null)
            {
                return new UserInfoViewModel
                {
                    UserName = user.UserName,
                    FullName = user.FullName,
                    Email = user.Email
                };
            }
            else
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
        }

        [Route("UserInfo")]
        public async Task<IHttpActionResult> PutUserInfo(UserInfoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await userManager.FindByNameAsync(model.UserName);

            if (model.UserName == null
                || (!string.Equals(User.Identity.GetUserName(), model.UserName, StringComparison.CurrentCultureIgnoreCase)
                    && !User.IsInRole("Administrators")))
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
            }

            user.FullName = model.FullName;
            user.Email = model.Email;

            IdentityResult result = await userManager.UpdateAsync(user);
            await user.SynchronizeJournalUser(dbContext);

            if (!result.Succeeded)
            {
                return this.GetErrorResult(result);
            }
            return Ok();
        }


        [Route("UserInfo/{userName?}")]
        public async Task<IHttpActionResult> DeleteUserInfo(string userName = null)
        {
            if (userName != null
                && !string.Equals(User.Identity.GetUserName(), userName, StringComparison.CurrentCultureIgnoreCase)
                && !User.IsInRole("Administrators"))
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
            }

            if ((string.IsNullOrWhiteSpace(userName)
                || string.Equals(User.Identity.GetUserName(), userName, StringComparison.CurrentCultureIgnoreCase))
                && User.IsInRole("Administrators"))
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Can't delete administrators."));
            }

            var user = await userManager.FindByNameAsync(userName ?? User.Identity.GetUserName());

            if (user != null)
            {
                var result = userManager.Delete(user);

                if (!result.Succeeded)
                {
                    this.GetErrorResult(result);
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
                }
                return Ok();
            }
            else
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid userName."));
        }

    }
}
