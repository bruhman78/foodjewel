﻿using FoodJewel.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{
    public class JournalUser
    {
        private DateTimeWrapper dtw = default(DateTime);
        private WeightMeasurement lastWeight = new WeightMeasurement();
        private HeightMeasurement height = new HeightMeasurement();

        public int Id { get; set; }
        [StringLength(255)]
        [Index(IsUnique = true)]
        public string UserName { get; set; }
        public string FullName { get; set; }
        [Index(IsUnique = true)]
        public string IdentityUserId { get; set; }
        public ApplicationUser IdentityUser { get; set; }
        public ICollection<JournalEntry> JournalEntries { get; set; }
        public ICollection<WeightEntry> WeightEntries { get; set; }
        public DateTime Birthday
        {
            get { return dtw; }
            set { dtw = value; }
        }
        [NotMapped]
        public DateTimeOffset BirthdayOffset
        {
            get { return dtw; }
            set { dtw = value; }
        }
        public HeightMeasurement Height
        {
            get { return height; }
            set { height = value ?? new HeightMeasurement(); }
        }
        public WeightMeasurement LastWeight
        {
            get { return lastWeight; }
            set { lastWeight = value??new WeightMeasurement(); }
        }
        public int CalorieGoal { get; set; }

    }
}