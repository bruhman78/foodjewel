﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create(IdentityFactoryOptions<ApplicationDbContext> options, IOwinContext context)
        {
            var instance =  new ApplicationDbContext();
            var container = context.Get<IUnityContainer>();
            if (container != null && !container.IsRegistered<ApplicationDbContext>())
                container.RegisterInstance(instance);
            return instance;
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<double>().Configure(config => config.HasPrecision(18, 9));
            modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(10, 4));
            //modelBuilder.Entity<JournalUser>().HasOptional(r=>r.IdentityUser).WithOptionalDependent().WillCascadeOnDelete(true);
            modelBuilder.Entity<JournalUser>().HasOptional(r => r.IdentityUser).WithMany().HasForeignKey(r=>r.IdentityUserId).WillCascadeOnDelete(true);
            modelBuilder.Entity<RefreshToken>().HasOptional(r => r.Client).WithMany().HasForeignKey(r => r.ClientId).WillCascadeOnDelete();
            modelBuilder.Entity<RefreshToken>().HasOptional(r => r.User).WithMany().HasForeignKey(r => r.UserId).WillCascadeOnDelete();
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<OAuthClient> Clients { get; set; }
        public virtual DbSet<JournalEntry> JournalEntries { get; set; }
        public virtual DbSet<JournalUser> JournalUsers { get; set; }
        public virtual DbSet<WeightEntry> WeightEntries { get; set; }
    }
}