﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{
    public class HeightMeasurement : Measurement, IFormattable
    {
        public void SetFt(decimal? ft, decimal? inch)
        {
            Format = MeasureTypeEnum.ft;
            Value = (ft.HasValue ? ft.Value * 12 : 0m)
                + (inch.HasValue ? inch.Value : 0m);
        }

        public void SetMeter(decimal? m)
        {
            Format = MeasureTypeEnum.m;
            Value = (m * 100) ?? 0m;
        }

        public void SetCentimeters(decimal? cm)
        {
            Format = MeasureTypeEnum.cm;
            Value = cm ?? 0m;
        }


        public decimal GetFt()
        {
            return Convert(false, Value) / 12;
        }


        public decimal GetM()
        {
            return Convert(true, Value) / 100;
        }

        public decimal GetCM()
        {
            return Convert(true, Value);
        }

        public override string ToString()
        {
            return ToString("G", CultureInfo.CurrentCulture);
        }

        public string ToString(string format)
        {
            return ToString(format, CultureInfo.CurrentCulture);
        }

        public string ToString(IFormatProvider formatProvider)
        {
            return ToString("G", formatProvider);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrWhiteSpace(format)) format = "G";
            if (formatProvider == null) formatProvider = CultureInfo.CurrentCulture;
            var culture = (formatProvider as CultureInfo) ?? CultureInfo.CurrentCulture;
            var region = new RegionInfo(culture.LCID);

            //working variable
            decimal major = 0m;

            switch (format.ToUpperInvariant())
            {
                case "G":
                    major = Convert(region.IsMetric, Value);
                    //ft & in are stored in inches.  Divide for display
                    if (!region.IsMetric)
                        major = major / 12;
                    else
                        major = major / 100;
                    return major.ToString("G", formatProvider) + (region.IsMetric ? " ft" : " m");
                case "X":
                    switch (Format)
                    {
                        case MeasureTypeEnum.ft:
                            return (Value / 12).ToString("G") + " ft";
                        case MeasureTypeEnum.cm:
                            return Value.ToString("G") + " cm";
                        case MeasureTypeEnum.m:
                            return (Value / 100).ToString("G") + " m";
                        default:
                            return "Invalid format";
                    }
                case "X1":
                    switch (Format)
                    {
                        case MeasureTypeEnum.ft:
                            major = Math.Floor(Value / 12);
                            var minor = Value % 12;
                            return string.Format("{0:F0} ft{1}", major, minor == 0 ? "" : string.Format(" {0:F0} in", minor));
                        case MeasureTypeEnum.cm:
                        case MeasureTypeEnum.m:
                            return Value.ToString("G") + " cm";
                        default:
                            return "Invalid format";
                    }
                default:
                    return "Invalid format specifier";
            }
        }

        private decimal Convert(bool convertToMetric, decimal val)
        {
            //Do we need to convert?
            if (convertToMetric != (Format == MeasureTypeEnum.m || Format == MeasureTypeEnum.cm))
            {
                //in to cm conversion factor = 2.54
                var factor = convertToMetric ?
                    2.54m :       //in to cm
                    1m / 2.54m;   //cm to in 
                val = val * factor;
            }
            return val;
        }
    }
}