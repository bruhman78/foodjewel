﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using FoodJewel.Models;
using FoodJewel.Models.Domain;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            await ValidateTicket(context, userManager, user, context.ClientId);
        }

        private static async Task ValidateTicket(BaseValidatingTicketContext<OAuthAuthorizationServerOptions> context, ApplicationUserManager userManager, ApplicationUser user, string clientId)
        {

            await user.SynchronizeJournalUser(context.OwinContext.Get<ApplicationDbContext>());

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);


            AuthenticationProperties properties = CreateProperties(user.UserName);
            properties.Dictionary.Add("as:client_id", clientId);

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
        }


        public override async Task GrantRefreshToken(
            OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            //var currentClient = context.OwinContext.Get<string>("as:client_id");
            var currentClient = context.ClientId;

            // enforce client binding of refresh token
            var dbContext = context.OwinContext.Get<ApplicationDbContext>();
            var client = dbContext.Clients.SingleOrDefault(r => r.Id == currentClient && r.Enabled == true);
            if (originalClient != currentClient && client != null)
            {
                context.SetError("unauthorized_client", "The client is not authorized for this application.");
                context.Rejected();
                return;
            }

            // chance to change authentication ticket for refresh token requests
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = await userManager.FindByIdAsync(context.Ticket.Identity.GetUserId());


            if (user == null)
            {
                context.SetError("invalid_grant", "The user cannot be found for refresh.");
                return;
            }

            await ValidateTicket(context, userManager, user, context.ClientId);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var dbContext = context.OwinContext.Get<ApplicationDbContext>();
            string Id, Secret;

            if (context.TryGetBasicCredentials(out Id, out Secret) ||
                context.TryGetFormCredentials(out Id, out Secret))
            {
                //Make sure client is registered and Enabled
                var client = dbContext.Clients.SingleOrDefault(r => r.Id == Id && r.Enabled == true);

                //var SecretHash = userManager.PasswordHasher.HashPassword(Secret);
                //if (client != null && client.SecretHash == SecretHash)
                if (client != null && client.Secret == Secret)
                {
                    // need to make the client_id available for later security checks
                    context.OwinContext.Set<string>("as:client_id", Id);
                    context.Validated();
                }
                else
                {
                    context.SetError("unauthorized_client", "Invalid client request.");
                    context.Rejected();
                }
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}