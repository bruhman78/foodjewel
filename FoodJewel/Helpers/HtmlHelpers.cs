﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodJewel.Helpers
{
    public static class HtmlHelpers
    {
        public static bool IsDebug(this HtmlHelper html)
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }
    }
}