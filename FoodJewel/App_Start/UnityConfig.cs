using FoodJewel.Code;
using FoodJewel.Models.Domain;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Practices.Unity;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel
{
    public static class UnityConfig
    {
        private static UnityContainer container;

        public static void RegisterComponents(HttpConfiguration configuration)
        {
            container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            configuration.DependencyResolver = new UnityDependencyResolver(container);
            configuration.MessageHandlers.Add(new UnityConfig.UnityMessageHandler());
        }


        public class UnityMessageHandler : DelegatingHandler
        {
            public UnityMessageHandler() : base() { }

            public UnityMessageHandler(HttpMessageHandler handler) : base(handler) { }

            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
            {
                var dbContext = request.GetOwinContext().Get<ApplicationDbContext>();
                var userManager = request.GetOwinContext().Get<ApplicationUserManager>();
                var scope = request.GetDependencyScope();
                var container = scope.GetService(typeof(IUnityContainer)) as IUnityContainer;
                request.GetOwinContext().Set<IUnityContainer>(container);

                if (dbContext != null && !container.IsRegistered<ApplicationDbContext>())
                    container.RegisterInstance(dbContext);
                if (userManager != null && !container.IsRegistered<ApplicationUserManager>())
                    container.RegisterInstance(userManager);

                return await base.SendAsync(request, cancellationToken);
            }
        }
    }
}