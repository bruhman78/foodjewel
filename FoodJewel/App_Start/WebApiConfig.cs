﻿using FoodJewel.Models.Domain;
using Microsoft.OData.Edm;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using System.Web.OData.Routing;
using System.Web.OData.Formatter;
using System.Web.OData.Formatter.Serialization;
using FoodJewel.Code;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();
            ODataRoute route = config.MapODataServiceRoute("odata", "odata", GetModel());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public static IEdmModel GetModel()
        {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            //MySql doesn't use DateTimeOffset, but we want them in the OData
            //Work around ignores the Edm type and makes a client side type.
            var journalEntryConfig = builder.EntitySet<JournalEntry>("JournalEntries");
            journalEntryConfig.EntityType.Ignore(r => r.Date);
            journalEntryConfig.EntityType.Property(r => r.DateOffset).Name = "Date";

            var usersConfig = builder.EntitySet<JournalUser>("JournalUsers");
            usersConfig.EntityType.Ignore(r => r.IdentityUser);
            //usersConfig.EntityType.Ignore(r => r.IdentityUserId);
            usersConfig.EntityType.Property(r=>r.IdentityUserId);
            usersConfig.EntityType.Ignore(r => r.Birthday);
            usersConfig.EntityType.Property(r => r.BirthdayOffset).Name = "Birthday";

            var weightEntryConfig = builder.EntitySet<WeightEntry>("WeightEntries");
            weightEntryConfig.EntityType.Ignore(r => r.Date);
            weightEntryConfig.EntityType.Property(r => r.DateOffset).Name = "Date";

            return builder.GetEdmModel();
        }
    }

}
