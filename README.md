# README #

FoodJewel is a weight and food journal tracking sample application.

It is currently incomplete.  But, contains the basic plumbing.

## Completed ##

* Authentication
    * ASP.Net Identity (with customization)
    * OWIN OAuth authentication
    * Refresh Tokens
* Database Schema
    * MySQL
    * Entity Framework (Code First with Migrations)
    * Single database for Identity and App Domain
* Web API (with OData)
    * Account Administration
        * Identity operations
        * Admin Role
    * Journal User
* Unit Tests
    * OWIN Test Server

## TODO ##

## How do I get set up? ##

* Requires MySQL database and NodeJS
* All other external dependencies should be updated through NuGet
* Modify connectionStrings.config
* Execute "update-database" in Package Manager console to create tables and seed data.
* From foodjewel project folder:
    * Install node modules by hand using "npm install", to confirm dependencies are correct.
    * NOTE:  msbuild errors may be corrected with:
```
#!c#

npm config set msvs_version 2013
```
    * git:// errors may be corrected (due to corporate firewalls) using 
```
#!c#

git config --global url.https://.insteadOf git://

```


##License##
This program follows the Apache License version 2.0 (<http://www.apache.org/licenses/> ) That means:

It allows you to:

* freely download and use this software, in whole or in part, for personal, company internal, or commercial purposes;
* use this software in packages or distributions that you create.

It forbids you to:

* redistribute any piece of our originated software without proper attribution;
* use any marks owned by me in any way that might state or imply that I (Thomas L Baker) endorse your distribution;
* use any marks owned by me in any way that might state or imply that you created this software in question.

It requires you to:

* include a copy of the license in any redistribution you may make that includes this software;
* provide clear attribution to me, (Thomas L Baker) for any distributions that include this software

It does not require you to:

* include the source of this software itself, or of any modifications you may have made to it, in any redistribution you may assemble that includes it;
* submit changes that you make to the software back to this software (though such feedback is encouraged).
* See License FAQ <http://www.apache.org/foundation/licence-FAQ.html> for more details.

##Feedback##
Please email any questions, feedback or code contibutions to [support@heliconsoftware.com](mailto:support@heliconsoftware.com)

Have fun!